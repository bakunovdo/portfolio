import React, { useEffect, useState } from "react";

import { useConfig } from "../hooks/useConfig";
import Typist from "react-typist";

import { Board } from "../components/board";
import { Contacts } from "../components/contacts";
import { Preloader } from "../components/preloader";
import { Error } from "../components/error";

import { LayoutContent } from "../ui/layout";

import { delay } from "../shared/utils";

import styles from "../styles/Home.module.css";

const timings = {
  loading: 1000,
  spin: 500,
};

export default function Home() {
  const [loading, setLoading] = useState(true);
  const config = useConfig();

  useEffect(() => {
    delay(false, timings.loading).then(setLoading);
  }, []);

  const isError = !loading && !config;

  if (loading) {
    return <Preloader fadeStart={timings.spin} />;
  }

  if (isError) {
    return <Error />;
  }

  return (
    <React.Fragment>
      <div className={styles.container}>
        <LayoutContent as="header" className={styles.header}>
          <h1 className={styles.title}>
            <Typist cursor={{ hideWhenDone: true }}>My name Bakunov Danila</Typist>
          </h1>
        </LayoutContent>
        <Contacts contacts={config?.contacts} />
        <LayoutContent as="main" className={styles.main}>
          <Board projects={config?.projects} />
        </LayoutContent>
      </div>
    </React.Fragment>
  );
}
