import styled from "@emotion/styled";

export const LayoutContent = styled.div`
  max-width: 1200px;
  margin: 0 auto;
`;

export const FSContainer = styled.div`
  width: 100vw;
  height: 100vh;
  overflow: hidden;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
`;
