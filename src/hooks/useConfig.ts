import { useEffect, useState } from "react";
import { TConfig } from "../types";

const API = "https://bakportfolio.herokuapp.com/api";

const csFetch = async (url: string, options = {}) => {
  const data = await fetch(url, options);
  return await data.json();
};

const fetchConfig = async (): Promise<TConfig | null> => {
  try {
    const projects = await csFetch(`${API}/projects`);
    const contacts = await csFetch(`${API}/contacts`);
    return { contacts, projects };
  } catch {
    return null;
  }
};

export const useConfig = () => {
  const [config, setConfig] = useState<null | TConfig>(null);

  useEffect(() => {
    if (!config) {
      fetchConfig().then(response => {
        if (response) setConfig(response);
      });
    }
  }, []);

  return config;
};
