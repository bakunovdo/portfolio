export interface TProject {
  title: string;
  previewphoto: string;
  tags: string;
  devices: "Desktop / Mobile" | "Desktop" | "Mobile";
  applink: string;
  sourcelink: string;
}

export type TContact = {
  name: string;
  link: string;
};

export interface TConfig {
  contacts: TContact[];
  projects: TProject[];
}
