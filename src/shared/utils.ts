export function delay<T>(data: T, timeout = 1500) {
  return new Promise<T>(resolve => {
    setTimeout(() => resolve(data), timeout);
  });
}
