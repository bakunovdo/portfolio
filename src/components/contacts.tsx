import styled from "@emotion/styled";
import { Fragment } from "react";
import { useMediaQuery } from "../hooks/useMediaQuery";
import { TContact } from "../types";

type Props = {
  contacts?: TContact[];
};

const newTabProps = {
  rel: "noreferrer noopener",
  target: "_blank",
};

const toContact = ({ name, link }: TContact, index: number) => (
  <a
    href={name === "email" ? `mailto:${link}` : link}
    key={index}
    className={name === "resume" ? "resume" : ""}
    {...newTabProps}
  >
    {name}
  </a>
);

export const Contacts: React.FC<Props> = ({ contacts }) => {
  if (!contacts) return null;

  const isSmallDevices = useMediaQuery("(max-width: 425px)");
  const [c1, c2, c3, c4, c5] = contacts;

  const SmallDevices = (
    <Fragment>
      <div>{[c2, c3, c4].map(toContact)}</div>
      <SmallContainer>{[c1, c5].map(toContact)}</SmallContainer>
    </Fragment>
  );

  return <Container>{isSmallDevices ? SmallDevices : contacts.map(toContact)}</Container>;
};

const Container = styled.div`
  display: flex;
  align-items: center;

  a {
    font-weight: bold;
    text-transform: uppercase;
    margin-right: 1rem;
    position: relative;
    font-size: 14px;

    &.resume {
      font-size: 20px;
    }

    :last-child {
      margin-right: 0;
    }

    :after {
      content: "";
      position: absolute;
      background-color: black;
      left: 0;
      bottom: -2px;
      height: 2px;
      width: 0;
      transition: width 0.2s ease-in-out;
    }

    :hover:after {
      width: 100%;
    }
  }

  @media (max-width: 425px) {
    flex-direction: column;
  }
`;

const SmallContainer = styled.div`
  display: flex;
  margin-left: 1.5rem;
  margin-top: 0.5rem;
`;
