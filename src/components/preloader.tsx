import React, { useEffect } from "react";
import styled from "@emotion/styled";

import { FSContainer } from "../ui/layout";

import { fadeOut, spin } from "../animations";
import { delay } from "../shared/utils";

type Props = {
  fadeStart?: number;
};

const stages: Record<string, string[]> = {
  false: ["spin 0.5s linear infinite"],
  true: ["spin 0.5s linear infinite", "fadeout 0.5s linear"],
};

export const Preloader: React.FC<Props> = ({ fadeStart }) => {
  const [isDone, setIsDone] = React.useState(false);

  useEffect(() => {
    if (fadeStart) {
      delay(true, fadeStart).then(setIsDone);
    }
  }, []);

  return (
    <FSContainer>
      <SpinContainer stage={isDone ? "true" : "false"}>
        <Image src="/assets/preloader.png"></Image>
      </SpinContainer>
    </FSContainer>
  );
};

const SpinContainer = styled.div<{ stage: string }>`
  animation: ${({ stage }) => stages[stage].join()};

  ${spin}
  ${fadeOut}
`;

const Image = styled.img`
  width: 200px;
  height: 200px;
`;
