import styled from "@emotion/styled";
import { FSContainer } from "../ui/layout";

export const Error = () => {
  return (
    <Container>
      <h2>Server is starting up, please wait a minute</h2>
      <br />
      <div>
        If the server has not started, please contact me:
        <a href="https://t.me/tbakunovdo" target="_blank" rel="noreferrer noopener">
          telegram
        </a>
      </div>
    </Container>
  );
};

const Container = styled(FSContainer)`
  padding: 1rem;
  text-align: center;
  a {
    text-decoration: underline;
  }
`;
