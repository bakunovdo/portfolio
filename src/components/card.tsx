import styled from "@emotion/styled";
import { TProject } from "../types";

const colors = ["#BCE7FD", "#F3C98B", "#DAA588", "#F56960"];

const toTag = (value: string, index: number) => (
  <SkillTag key={index} bgc={colors[index]}>
    <Content>{value}</Content>
  </SkillTag>
);

export const Card = (props: TProject) => {
  return (
    <CardStyled>
      <DeviceTag bgc="#9adfd4">{props.devices}</DeviceTag>
      <TagsList>{props.tags.split(",").map(toTag)}</TagsList>
      <a href={props.applink} target="_blank" rel="noreferrer noopener">
        <Image src={props.previewphoto} />
      </a>
      <CardInfo>
        <CardMain>
          <Title>
            <h3>{props.title}</h3>
          </Title>
          <Link href={props.sourcelink} target="_blank" rel="noreferrer noopener">
            <Controls>
              <span>Source code</span>
            </Controls>
          </Link>
        </CardMain>
      </CardInfo>
    </CardStyled>
  );
};

const CardStyled = styled.div`
  width: 302px;
  height: 300px;
  border-radius: 4px;
  border: 1px solid #e6e6e6;
  overflow: hidden;
  display: flex;
  flex-direction: column;
  box-shadow: 0 0 20px rgb(0 0 0 / 10%);
  transition: all ease-in-out 0.1s;

  position: relative;

  :hover {
    transform: scale(1.04);
  }
`;

const Image = styled.img`
  max-height: 250px;
  height: 250px;
  width: 100%;
  object-fit: cover;
  background-color: #ccc;
  cursor: pointer;
  transition: all ease-in-out 0.25s;
`;

const CardInfo = styled.div`
  flex-grow: 1;
  flex-direction: column;
  justify-content: space-between;
  display: flex;
  padding: 0.5rem 1rem;
`;

const CardMain = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const Controls = styled.div`
  display: flex;
  height: fit-content;
  font-size: 12px;
  align-items: center;
  justify-content: space-between;
  padding: 6px 0.4rem 4px;
  border: 1px solid #000;
  transition: all ease-in-out 0.2s;

  :hover {
    background-color: black;
    color: white;
  }
`;

const Link = styled.a`
  display: flex;
`;

const Tag = styled.div<{ bgc?: string }>`
  padding: 0.4rem 0.5rem 0.2rem;
  font-size: 12px;
  background-color: ${({ bgc }) => bgc || "unset"};
`;

const SkillTag = styled(Tag)`
  padding: 0.4rem 1rem 0.2rem 0.6rem;
  margin: -4px;
  transform: skew(-25deg);
  :last-child {
    padding: 0.4rem 0.6rem 0.2rem 0.6rem;
  }
`;

const Content = styled.div`
  transform: skew(25deg);
`;

const DeviceTag = styled(Tag)`
  position: absolute;
  right: 0;
`;

const Title = styled.div`
  height: 18px;
  max-width: 160px;

  h3 {
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
  }
`;

const TagsList = styled.div`
  display: flex;
  position: absolute;
  bottom: 60px;
  right: 50%;
  width: fit-content;
  transform: translateX(50%);
  filter: drop-shadow(1px 1px 3px rgba(0, 0, 0, 0.7));
`;
