import styled from "@emotion/styled";
import { LayoutContent } from "../ui/layout";

export const Footer = () => {
  return (
    <FooterStyled>
      <Content>
        Icons made by{" "}
        <a href="https://www.flaticon.com/authors/dave-gandy" title="Dave Gandy">
          Dave Gandy
        </a>{" "}
        from{" "}
        <a href="https://www.flaticon.com/" title="Flaticon">
          www.flaticon.com
        </a>
      </Content>
    </FooterStyled>
  );
};

const FooterStyled = styled.div`
  margin-top: 1rem;
  display: flex;
  align-items: center;
  justify-content: center;
  height: 75px;
  width: 100%;
  border-top: 1px solid #ececec;
  box-shadow: 0 -5px 20px rgb(0 0 0 / 10%);

  a {
    text-decoration: underline;
  }
`;

const Content = styled(LayoutContent)`
  padding: 0 1rem;
  text-align: center;
`;
