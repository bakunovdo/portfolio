import styled from "@emotion/styled";
import { TProject } from "../types";
import { Card } from "./card";

type Props = {
  projects?: TProject[];
};

export const Board: React.FC<Props> = ({ projects }) => {
  if (!projects) return null;

  return (
    <Container>
      {projects.map((props, index) => (
        <Card {...props} key={index} />
      ))}
    </Container>
  );
};

const Container = styled.div`
  padding: 1rem 0;
  display: grid;
  grid-template-columns: repeat(auto-fill, 300px);
  grid-gap: 2rem 1.5rem;
  justify-content: center;
  width: 100%;
`;
